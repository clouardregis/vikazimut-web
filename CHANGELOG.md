# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/index.html),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.2.0]

- Updated to Symfony 7.2

## [3.0.1]

- Updated to Symfony 6.4
- Added Rogaine with imported GPX files

## [2.3.3]

### Added

- Changed composer.phar 
- Added profile annotation page for experts

## [2.3.2]

### Fixed

- Bug on create course form

### Revised

- Generate CSV file for event

## [2.3.0]

### Added

- Course preferred characteristics: displicine, course type, format and validation mode

## [2.2.0]

### Added

- Mailing to all planners
- QR code for loading and starting the course

### Changed

- Look and Feel for the menu bar

## [2.1.0]

### Added

- Animation of the tracks
- statistics: Number of route downloads

### Fixed

- Responsiveness on smartphones

### Changed

- Look and Feel page of list of courses

### Updated

- Deprecated authentication

## [2.0.0]  2021-06-06

### Fixed

- Fix a lot of bugs

### Added

- Events

### Changed

- Result statistics