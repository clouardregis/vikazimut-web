<?php

namespace App\Entity;

use App\Repository\EventCourseRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventCourseRepository::class)]
class EventCourse
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'event_courses')]
    private ?Event $event;

    #[ORM\ManyToOne(targetEntity: Course::class, inversedBy: 'event_courses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Course $course;

    #[ORM\Column(type: Types::INTEGER)]
    private int $format;

    #[ORM\Column(type: Types::INTEGER)]
    private int $mispunchPenalty;

    #[ORM\Column(type: Types::INTEGER)]
    private int $overtimePenalty;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private int $maxTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getMispunchPenalty(): ?int
    {
        return $this->mispunchPenalty;
    }

    public function setMispunchPenalty(int $mispunchPenalty): self
    {
        $this->mispunchPenalty = $mispunchPenalty;

        return $this;
    }

    public function getOvertimePenalty(): ?int
    {
        return $this->overtimePenalty;
    }

    public function setOvertimePenalty(int $overtimePenalty): self
    {
        $this->overtimePenalty = $overtimePenalty;

        return $this;
    }

    public function getMaxTime(): ?int
    {
        return $this->maxTime;
    }

    public function setMaxTime(int $maxTime): self
    {
        $this->maxTime = $maxTime;

        return $this;
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        $participantMakeEventCourseRepository = $entityManager->getRepository(ParticipantMakeEventCourse::class);
        /** @var Participant $participant */
        foreach ($this->event->getParticipants() as $participant) {
            /** @var ParticipantMakeEventCourse $participantMakeEventCourse */
            $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $this, "participant" => $participant));
            $participantMakeEventCourse?->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function generateParticipantMakeEventCourse(EntityManagerInterface $entityManager): void
    {
        $participants = $this->event->getParticipants();
        foreach ($participants as $participant) {
            $participantMakeEventCourse = new ParticipantMakeEventCourse();
            $participantMakeEventCourse->setParticipant($participant);
            $participantMakeEventCourse->setEventCourse($this);
            $participantMakeEventCourse->setTrack(null);
            $entityManager->persist($participantMakeEventCourse);
        }
        $entityManager->flush();
    }
}