<?php

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParticipantRepository::class)]
class Participant
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $nickname;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: "participants")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Event $event;

    public static function checkNicknameValidity($nickname): bool
    {
        if (strlen($nickname) > 30) {
            return false;
        }
        if (!(preg_match("/^([\da-zA-Zá-úÁ-Ú_.' @\-]+)$/", $nickname))) {
            return false;
        }
        return true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        $participantMakeEventCourseRepository = $entityManager->getRepository(ParticipantMakeEventCourse::class);
        foreach ($this->event->getEventCourses() as $eventCourse) {
            $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $this));
            $participantMakeEventCourse?->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    static public function checkUnregisteredNickname($nickname, Event $event): bool
    {
        foreach ($event->getParticipants() as $participant) {
            if ($nickname === $participant->getNickname()) {
                return false;
            }
        }
        return true;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function modifyNickname(Event $event, string $newNickname, EntityManagerInterface $entityManager): ?string
    {
        if (!$this->checkUnregisteredNickname($newNickname, $event)) {
            return "Nickname already registered";
        }
        foreach ($event->getParticipants() as $participant) {
            if (strtolower($this->nickname) === strtolower($participant->getNickname())) {
                $participant->setNickname($newNickname);
                $entityManager->persist($participant);
                $entityManager->flush();
                return null;
            }
        }
        return "Unknown nickname";
    }


    static public function addParticipant(string $nickname, Event $event, EntityManagerInterface $entityManager): bool
    {
        if (Participant::checkNicknameValidity($nickname)) {
            if (Participant::checkUnregisteredNickname($nickname, $event)) {
                $participant = new Participant();
                $participant->setNickname($nickname);
                $participant->setEvent($event);
                $entityManager->persist($participant);
                $entityManager->flush();
                $event->getParticipants()->add($participant);
                /** @var EventCourse $eventCourse */
                foreach ($event->getEventCourses() as $eventCourse) {
                    $participantMakeEventCourse = new ParticipantMakeEventCourse();
                    $participantMakeEventCourse->setParticipant($participant);
                    $participantMakeEventCourse->setEventCourse($eventCourse);
                    $participantMakeEventCourse->setTrack(null);
                    $courseId = $eventCourse->getCourse()->getId();
                    $track = $entityManager->getRepository(Track::class)->findOneBy(
                        [
                            'name' => $nickname,
                            'course' => $courseId
                        ],
                    );
                    if ($track) {
                        $participantMakeEventCourse->setTrack($track);
                    }
                    $entityManager->persist($participantMakeEventCourse);
                }
                $entityManager->flush();
            }
            return true;
        } else {
            return false;
        }
    }
}
