<?php

namespace App\Entity;

use App\Repository\MissingControlPointRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MissingControlPointRepository::class)]
class MissingControlPoint
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::STRING, length: 20)]
    private ?string $controlPointId;

    #[ORM\Column(type: Types::INTEGER)]
    private ?int $courseId;

    #[ORM\ManyToOne(targetEntity: Course::class, inversedBy: "missingControlPoints")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Course $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getControlPointId(): ?string
    {
        return $this->controlPointId;
    }

    public function setControlPointId(string $controlPointId): self
    {
        $this->controlPointId = $controlPointId;

        return $this;
    }

    public function getCourseId(): ?int
    {
        return $this->courseId;
    }

    public function setCourseId(int $courseId): self
    {
        $this->courseId = $courseId;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }
}
