<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

define("CHAMPIONSHIP_EVENT_TYPE", 0);
define("POINT_EVENT_TYPE", 1);
define("CUMULATIVE_TIME_EVENT_TYPE", 2);

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: "events")]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $creator;

    /**
     * 0: Championship
     * 1: Points
     * 2: Cumulative time
     */
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $type;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\OneToMany(targetEntity: EventCourse::class, mappedBy: "event", orphanRemoval: true)]
    private Collection $eventCourses;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\OneToMany(targetEntity: Participant::class, mappedBy: "event", orphanRemoval: true)]
    private Collection $participants;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEventCourses(): Collection
    {
        return $this->eventCourses;
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        /** @var EventCourse $eventCourse */
        foreach ($this->getEventCourses() as $eventCourse) {
            $eventCourse->remove($entityManager);
        }
        /** @var Participant $participant */
        foreach ($this->getParticipants() as $participant) {
            $participant->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function updateEventScores(EntityManagerInterface $entityManager): void
    {
        if ($this->type === CHAMPIONSHIP_EVENT_TYPE) {
            // The score is 1000 * best_min_time / time
            foreach ($this->eventCourses as $eventCourse) {
                $participantTimeInMs = [];
                $bestMinTimeInMs = INF;
                foreach ($this->participants as $participant) {
                    $participantMakeEventCourse = $entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    // Compute time (and best min time) for all participants including penalties
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $participantMakeEventCourse->updatePenaltiesAndPointsFromTrack();
                        $timeInMs = $participantMakeEventCourse->getTrack()->getTotalTime()
                            + $participantMakeEventCourse->getMispunchCount() * $participantMakeEventCourse->getEventCourse()->getMispunchPenalty() * 1000
                            + $participantMakeEventCourse->getOvertimeCount() * $participantMakeEventCourse->getEventCourse()->getOvertimePenalty() * 1000;
                        if ($bestMinTimeInMs > $timeInMs) {
                            $bestMinTimeInMs = $timeInMs;
                        }
                        $participantTimeInMs[$participant->getId()] = $timeInMs;
                    }
                }
                // Compute the score for each participant including penalties
                foreach ($this->participants as $participant) {
                    $participantMakeEventCourse = $entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $timeInMs = $participantTimeInMs[$participant->getId()];
                        if (isset($timeInMs) && $timeInMs > 0) {
                            $participantMakeEventCourse->setScore(1000 * $bestMinTimeInMs / $timeInMs);
                        }
                    }
                }
            }
        } else { // POINT_EVENT_TYPE or CUMULATIVE_TIME_EVENT_TYPE
            foreach ($this->eventCourses as $eventCourse) {
                foreach ($this->participants as $participant) {
                    $participantMakeEventCourse = $entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $participantMakeEventCourse->updatePenaltiesAndPointsFromTrack();
                    }
                }
            }
        }
        $entityManager->flush();
    }

    public
    function computeRanking($participantMakeEventCourseRepository): array
    {
        $participants = $this->participants;
        $eventCourses = $this->eventCourses;
        $totalScores = [];
        $totalTimes = [];
        $rankings = [];
        $rankings["rank"] = array();
        if ($participants[0] !== null and $eventCourses[0] !== null) {
            for ($i = 0; $i < sizeof($participants); $i++) {
                $totalScores[$i] = 0;
                $totalTimes[$i] = 0;
                $totalGrossScore = 0;
                $totalPenalty = 0;
                $rankings["progress"][$i] = 0;
                $rankings["event_course"][$i] = array();
                $rankings["modified"][$i] = array();
                $rankings["punch_times"][$i] = array();
                foreach ($eventCourses as $eventCourse) {
                    /** @var ParticipantMakeEventCourse $participantMakeEventCourse */
                    $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participants->get($i)));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $eventCourseScore = $participantMakeEventCourse->getScore();
                        $eventCourseTime = $participantMakeEventCourse->getTrack()->getTotalTime();
                        $eventCourseTimeWithPenalty = $eventCourseTime;
                        $punchTimes = $participantMakeEventCourse->getTrack()->getPunches();
                        if ($this->type === CHAMPIONSHIP_EVENT_TYPE || $this->type === CUMULATIVE_TIME_EVENT_TYPE) {
                            $penalties = $participantMakeEventCourse->getMispunchCount() * $participantMakeEventCourse->getEventCourse()->getMispunchPenalty() * 1000
                                + $participantMakeEventCourse->getOvertimeCount() * $participantMakeEventCourse->getEventCourse()->getOverTimePenalty() * 1000;
                            $eventCourseTimeWithPenalty += $penalties;
                            $grossScore = $eventCourseScore;
                        } else { // $this->type === POINT_EVENT_TYPE
                            $penalties = $participantMakeEventCourse->getMispunchCount() * $participantMakeEventCourse->getEventCourse()->getMispunchPenalty()
                                + $participantMakeEventCourse->getOvertimeCount() * $participantMakeEventCourse->getEventCourse()->getOverTimePenalty();
                            $grossScore = $eventCourseScore + $penalties;
                        }
                        $rankings["progress"][$i] += 1;
                        $totalScores[$i] += $eventCourseScore;
                        $totalTimes[$i] += $eventCourseTimeWithPenalty;

                        $punchTimeValues = [];
                        foreach ($punchTimes as $punchTime) {
                            $punchTimeValues[] = $punchTime["punchTime"];
                        }

                        $rankings["event_course"][$i][] = array(
                            $eventCourseScore,
                            $participantMakeEventCourse->getMispunchCount(),
                            $participantMakeEventCourse->getOvertimeCount(),
                            $eventCourseTime,
                            $eventCourseTimeWithPenalty,
                            $punchTimeValues,
                            $grossScore,
                            $participantMakeEventCourse->getEventCourse()->getMispunchPenalty(),
                            $participantMakeEventCourse->getEventCourse()->getOverTimePenalty(),
                        );
                        $totalGrossScore += $grossScore;
                        $totalPenalty += $penalties;
                    } else {
                        $rankings["event_course"][$i][] = array("-", "-", "-", "-", "-", [], "-", "-", "-");
                    }
                    $rankings["modified"][$i][] = $participantMakeEventCourse->isModified();
                }
                $rankings["total_score"][$i] = $totalScores[$i];
                $rankings["total_time"][$i] = $totalTimes[$i];
                $rankings["gross_score"][$i] = $totalGrossScore;
                $rankings["total_penalty"][$i] = $totalPenalty;
            }

            // Compute ranking
            if ($this->type === CHAMPIONSHIP_EVENT_TYPE || $this->type === POINT_EVENT_TYPE) {
                // Sort based on score and then on total time
                $results = array();
                for ($i = 0; $i < count($totalScores); $i++) {
                    $results[$i] = array($totalScores[$i], $totalTimes[$i]);
                }
                uasort($results, function ($a, $b): int {
                    if ($a[0] == $b[0]) {
                        if ($a[1] == $b[1]) {
                            return 0;
                        }
                        return ($a[1] < $b[1]) ? -1 : 1;
                    }
                    return ($a[0] > $b[0]) ? -1 : 1;
                });

                foreach ($results as $key => $value) {
                    $rankings["rank"][] = $key;
                }
            } elseif ($this->type === CUMULATIVE_TIME_EVENT_TYPE) {
                $results = array();
                for ($i = 0; $i < count($totalScores); $i++) {
                    $results[$i] = array($rankings["progress"][$i], $totalTimes[$i]);
                }
                // Sort result on progress and then total times
                uasort($results, function ($b, $a): int {
                    if ($a[0] == $b[0]) {
                        if ($a[1] == $b[1]) {
                            return 0;
                        }
                        return ($a[1] > $b[1]) ? -1 : 1;
                    }
                    return ($a[0] < $b[0]) ? -1 : 1;
                });

                foreach ($results as $key => $value) {
                    $rankings["rank"][] = $key;
                }
            }
        } else {
            for ($i = 0; $i < sizeof($participants); $i++) {
                $rankings["rank"][] = $i;
                $rankings["total_score"][] = 0;
                $rankings["gross_score"][] = 0;
                $rankings["total_penalty"][] = 0;
                $rankings["progress"][] = 0;
                $rankings["total_time"][] = 0;
                $rankings["punch_time"][] = [];
                $rankings["event_course"][] = [];
                $rankings["modified"][] = false;
            }
        }
        return $rankings;
    }
}
