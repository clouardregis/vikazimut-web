<?php

namespace App\Repository;

use App\Entity\EventCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventCourse[]    findAll()
 * @method EventCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventCourse::class);
    }
}
