<?php

namespace App\Model;

use App\Entity\Course;
use App\Entity\Track;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class CreateTrack
{
    private Track $track;
    private mixed $data;

    public function __construct(mixed $data)
    {
        $this->track = new Track();
        $this->data = $data;
    }

    public function addTrack(EntityManagerInterface $entityManager): ?int
    {
        if (!filter_var($this->data->courseId, FILTER_VALIDATE_INT)) {
            return false;
        }
        $course = $entityManager->getRepository(Course::class)->find(intval($this->data->courseId));
        if ($course == null) {
            return false;
        }
        $nickname = $this->data->orienteer;
        // Keep this for compatibility purposes
        if ($nickname == null) {
            $nickname = "*****";
        }
        if (strlen($nickname) > 60) {
            return false;
        }
        $format = $this->data->format;
        // Keep this for compatibility purposes: since 2023
        if ($format == null) {
            $format = 0;
        }
        // Avoid adding twice the same nickname
        $this->track->setName($nickname);
        $this->track->setDate(new DateTime('now'));
        $this->track->setTotalTime($this->data->totalTime);
        $this->track->setFormat($format);
        $this->track->setCourse($course);
        $punches = json_decode(json_encode($this->data->controlPoints), associative: true);
        $this->track->setPunches($punches);
        $this->track->setTrace($this->data->trace);
        $this->track->setImported(!empty($this->data->imported) && $this->data->imported);
        if (isset($this->data->runCount)) {
            $this->track->setRunCount($this->data->runCount);
        } else {
            $this->track->setRunCount(null);
        }
        $this->track->setCheating(Track::detectCheating($punches, $this->data->trace, $course->getXml()));
        if ($format > 0) {
            $this->track->setScore(Track::computeScore($punches, $course->getXml()));
        } else {
            $this->track->setScore(Track::computeNumberOfPunchedCheckpoints($punches));
        }

        $existingTrack = $entityManager->getRepository(Track::class)->findOneBy([
            "name" => $this->track->getName(),
            "course" => $course,
            "format" => $this->track->getFormat(),
        ]);

        if ($existingTrack == null) {
            try {
                $entityManager->persist($this->track);
                $entityManager->flush();
                return $this->track->getId();
            } catch (Exception) {
                return null;
            }
        } else {
            try {
                if ($this->track->getImported() && self::isRogaine($this->track, $existingTrack)) {
                    $baseTime1 = self::getLastPunchTime($existingTrack);
                    $baseTime2 = self::getFirstPunchTime($this->track);
                    $newTrace = self::mergeTrace($existingTrack->getTrace(), $this->track->getTrace(), $baseTime1, $baseTime2);
                    $newPunches = self::mergePunches($existingTrack->getPunches(), $this->track->getPunches(), $baseTime1, $baseTime2);
                    $existingTrack->setDate($this->track->getDate());
                    $existingTrack->setTotalTime($baseTime1 + $this->track->getTotalTime());
                    $existingTrack->setPunches($newPunches);
                    $existingTrack->setTrace($newTrace);
                    $run = $this->data->runCount ?? 1;
                    $existingTrack->setRunCount($run + 1);
                    $existingTrack->setCheating($this->track->isCheating() || $existingTrack->isCheating());
                    $existingTrack->setScore($this->track->getScore() + $existingTrack->getScore());
                } else { // Replace
                    $existingTrack->setDate($this->track->getDate());
                    $existingTrack->setTotalTime($this->track->getTotalTime());
                    $existingTrack->setPunches($this->track->getPunches());
                    $existingTrack->setTrace($this->track->getTrace());
                    $existingTrack->setRunCount($this->track->getRunCount());
                    $existingTrack->setCheating($this->track->isCheating());
                    $existingTrack->setScore($this->track->getScore());
                }
                $entityManager->flush();
                return  $existingTrack->getId();
            } catch (Exception) {
                return null;
            }
        }
    }

    public static function isRogaine(Track $newTrack, Track $existingTrack): bool
    {
        if ($newTrack->getScore() <= 0 || $existingTrack->getScore() <= 0) {
            return false;
        }
        $existingPunches = $existingTrack->getPunches();
        $lastPunch = $existingPunches[0];
        foreach ($existingPunches as $existingPunch) {
            if ($existingPunch["punchTime"] >= 0 && $existingPunch["punchTime"] > $lastPunch["punchTime"]) {
                $lastPunch = $existingPunch;
            }
        }
        $newPunches = $newTrack->getPunches();
        $firstPunch = null;
        foreach ($newPunches as $newPunch) {
            if ($newPunch["punchTime"] >= 0 && ($firstPunch == null || $newPunch["punchTime"] <= $firstPunch["punchTime"])) {
                $firstPunch = $newPunch;
            }
        }
        if ($lastPunch["controlPoint"] != $firstPunch["controlPoint"]) {
            return false;
        }
        for ($i = 1; $i < count($newPunches) - 1; $i++) {
            $newPunch = $newPunches[$i];
            for ($j = 1; $j < count($existingPunches) - 1; $j++) {
                $existingPunch = $existingPunches[$j];
                if ($existingPunch["punchTime"] >= 0
                    && $existingTrack != $lastPunch
                    && $newPunch["punchTime"] >= 0
                    && $newPunch != $firstPunch
                    && $newPunch["controlPoint"] == $existingPunch["controlPoint"]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function getLastPunchTime(Track $track): int
    {
        $punches = $track->getPunches();
        $lastPunch = $punches[0];
        foreach ($punches as $existingPunch) {
            if ($existingPunch["punchTime"] >= 0 && $existingPunch["punchTime"] >= $lastPunch["punchTime"]) {
                $lastPunch = $existingPunch;
            }
        }
        return $lastPunch["punchTime"];
    }

    public static function getFirstPunchTime(Track $track): int
    {
        $punches = $track->getPunches();
        $firstPunch = null;
        foreach ($punches as $punch) {
            if ($punch["punchTime"] >= 0 && ($firstPunch == null || $punch["punchTime"] < $firstPunch["punchTime"])) {
                $firstPunch = $punch;
            }
        }
        return $firstPunch["punchTime"];
    }

    public static function mergePunches(array $punches1, array $punches2, int $baseTime1, int $baseTime2): array
    {
        $newPunches = [];
        for ($i = 0; $i < count($punches1); $i++) {
            if ($punches1[$i]["punchTime"] >= 0) {
                $newPunches[] = ["punchTime" => $punches1[$i]["punchTime"], "controlPoint" => $punches1[$i]["controlPoint"]];
            } else if ($punches2[$i]["punchTime"] >= 0) {
                $newPunches[] = ["punchTime" => $punches2[$i]["punchTime"] - $baseTime2 + $baseTime1, "controlPoint" => $punches2[$i]["controlPoint"]];
            } else {
                $newPunches[] = ["punchTime" => -1, "controlPoint" => $punches1[$i]["controlPoint"]];
            }
        }
        return $newPunches;
    }

    public static function mergeTrace(?string $route1, ?string $route2, int $baseTime1, int $baseTime2): string
    {
        $waypoints1 = Track::extractWaypointsFromRouteString($route1);
        $finalWaypoints = [];
        for ($i = 0; $i < count($waypoints1); $i++) {
            if ($waypoints1[$i][2] <= $baseTime1) {
                $finalWaypoints[] = [$waypoints1[$i][0], $waypoints1[$i][1], $waypoints1[$i][2], $waypoints1[$i][3]];
            } else {
                break;
            }
        }

        $waypoints2 = Track::extractWaypointsFromRouteString($route2);
        $start = 0;
        for ($i = 0; $i < count($waypoints2); $i++) {
            if ($waypoints2[$i][2] >= $baseTime2) {
                $start = $i;
                break;
            }
        }
        for ($i = $start; $i < count($waypoints2); $i++) {
            $finalWaypoints[] = [$waypoints2[$i][0], $waypoints2[$i][1], $waypoints2[$i][2] - $baseTime2 + $baseTime1, $waypoints2[$i][3]];
        }
        return Track::WaypointsToString($finalWaypoints);
    }
}