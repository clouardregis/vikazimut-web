<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Track;
use App\Entity\User;
use App\Model\SupermanTrack;
use App\Model\TrackStatistics;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use function simplexml_load_string;

class CourseController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/worldmap', methods: ['GET'])]
    public function worldMap(): Response
    {
        // Kept for legacy purpose
        return $this->redirect("/web/#/worldmap");
    }

    #[Route('/courses/{course_id}/tracks', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function tracks(int $course_id): Response
    {
        // Kept for legacy purpose
        return $this->redirect("/web/#/course/tracks/$course_id");
    }

    #[Route('/api/routes/nearby/{lat}-{lng}', requirements: ['lat' => '{\-?\d+(\.\d+)?}', 'lng' => '{\-?\d+(\.\d+)?}'], methods: ['GET'])]
    public function nearbyCourses_old(string $lat, string $lng, EntityManagerInterface $entityManager): Response
    {
        return $this->get_all_near_by_courses(500, $lat, $lng, $entityManager);
    }

    #[Route('/api/routes/nearby/{radius}-{lat}-{lng}', requirements: ['radius' => '\d+', 'lat' => '{\-?\d+(\.\d+)?}', 'lng' => '{\-?\d+(\.\d+)?}'], methods: ['GET'])]
    public function get_all_near_by_courses(int $radius, string $lat, string $lng, EntityManagerInterface $entityManager): Response
    {
        $latitude = self::_removeStartAndEndCurlyBraces($lat);
        $longitude = self::_removeStartAndEndCurlyBraces($lng);
        $allCourses = $entityManager->getRepository(Course::class)->findAll();
        $closeCourses = [];
        if (abs($latitude) <= 90 && abs($longitude) <= 180) {
            foreach ($allCourses as $course) {
                $distance = TrackStatistics::distanceInMeters($latitude, $longitude, floatval($course->getLatitude()), floatval($course->getLongitude()));
                if ($distance < $radius) {
                    $closeCourses[] = ["distance" => $distance, "course" => $course];
                }
            }
        }
        $courses = [];
        foreach ($closeCourses as $course) {
            $courseData = $course["course"];
            if ($courseData->isOpen() && !$courseData->isPrivate()) {
                $info = [
                    "id" => $courseData->getId(),
                    "name" => $courseData->getName(),
                    "tracks" => count($courseData->getOrienteer()),
                ];
                $info["distance"] = intval($course["distance"]);
                if ($courseData->getClub() !== null) {
                    $info["club"] = $courseData->getClub();
                }
                if ($courseData->getClubURL() !== null) {
                    $info["cluburl"] = $courseData->getClubURL();
                }
                if ($courseData->getTouristic()) {
                    $info["touristic"] = 1;
                }
                if ($courseData->getPrintable()) {
                    $info["printable"] = true;
                }
                if ($courseData->getDiscipline()) {
                    $info["discipline"] = $courseData->getDiscipline();
                }
                $courses[] = $info;
            }
        }
        return new JsonResponse(
            $courses,
            Response::HTTP_OK
        );
    }

    #[Route('/api/courses/detail/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function course_details(int $course_id, TrackStatistics $statistics, EntityManagerInterface $entityManager): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }

        $idCreator = $course->getCreator();
        $user = $entityManager->getRepository(User::class)->find($idCreator);
        $date = $course->getCreatedAt();
        $isTouristRoute = $course->getTouristic();
        $xml = $course->getXml();
        $lengths = $statistics->computeLegLengths(simplexml_load_string($xml));
        return new JsonResponse(
            [
                "name" => $course->getName(),
                "club_name" => $course->getClub(),
                "club_url" => $course->getClubURL(),
                "creator" => $user->getUsername(),
                "date" => $date->format("c"),
                "length" => $course->getLength(),
                "checkpointCount" => count($lengths),
                "touristRoute" => $isTouristRoute,
                "discipline" => $course->getDiscipline(),
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/courses/info/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    function course_info(int $course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        /** @var User $user */
        $user = $this->security->getUser();
        $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
        if ($course->isPrivate() || $course->isClosed()) {
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }

        if (!$course->isHidden()) {
            $hiddenLevel = 0;
        } else if ($isGranted) {
            $hiddenLevel = 1;
        } else {
            $hiddenLevel = 2;
        }
        return new JsonResponse(
            [
                'name' => $course->getName(),
                'kml' => $course->getKml(),
                'image' => strval($course_id),
                'latitude' => $course->getLatitude(),
                'longitude' => $course->getLongitude(),
                'discipline' => $course->getDiscipline(),
                'format' => $course->getCourseFormat(),
                'hiddenLevel' => $hiddenLevel,
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/courses/checkpoints/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    function course_checkpoints(int $course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }

        $xmlContentsAsString = $course->getXml();
        $checkpoints = Course::extractCheckpointsFromXML(simplexml_load_string($xmlContentsAsString));
        return new JsonResponse(
            $checkpoints,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/courses/image/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function get_image(int $course_id, EntityManagerInterface $entityManager): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }
        $filename = $course->getImage();
        $fileExtension = pathinfo($filename, PATHINFO_EXTENSION);
        $contentType = "image/";
        if ($fileExtension == "png") {
            $contentType .= "png";
        } else {
            $contentType .= "jpeg";
        }
        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $course_id . "." . $fileExtension);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Length', filesize($filename));
        $response->headers->set('Cache-Control', 'no-cache, must-revalidate');
        $response->setEtag(strval($course->getCreatedAt()->getTimeStamp()));
        $response->setLastModified($course->getCreatedAt());
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    #[Route('/api/courses/track/{track_id}', requirements: ['track_id' => '\d+'], methods: ['GET'])]
    function get_course_track($track_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $track = $entityManager->getRepository(Track::class)->find($track_id);
        if ($track === null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }
        $course = $track->getCourse();
        if ($course === null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }
        $gpxTrace = Track::extractWaypointsFromRouteString($track->getTrace());
        return new JsonResponse(
            $gpxTrace,
            Response::HTTP_OK
        );
    }

    #[Route('/api/courses/create_superman_track/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    function create_superman_track(int $course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course === null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($course_id);
        $bestRoute = new SupermanTrack();
        $bestRoute->getBestRouteFromTrackSplits($tracks);

        return new JsonResponse(
            [
                'totalTime' => $bestRoute->getTotalTime(),
                'punchTimes' => $bestRoute->getPunchTimes(),
                'route' => $bestRoute->getRoute(),
            ],
            Response::HTTP_OK
        );
    }

    #[Route('/api/courses/tracks/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    function course_tracks(int $course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course === null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($course_id);
        $routes = [];
        foreach ($tracks as $track) {
            $score = $track->getScore();
            // Keep this for sake of backward compatibility: since (6/04/2024)
            if ($score < 0) {
                $score = Track::computeNumberOfPunchedCheckpoints($track->getPunches());
            }
            $routes[] = [
                $track->getId(),
                $track->getName(),
                $track->getTotalTime(),
                $track->getPunches(),
                $track->getFormat(),
                $track->getImported(),
                $track->isCheating(),
                $score,
                $track->getRunCount(),
            ];
        }
        return new JsonResponse(
            $routes,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/courses/track-nicknames/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    function course_track_nicknames(int $course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course === null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($course->isPrivate() || $course->isClosed()) {
            /** @var User $user */
            $user = $this->security->getUser();
            $isGranted = $user != null && ($course->getCreator() === $user || $user->isAdmin());
            if (!$isGranted) {
                return new JsonResponse(
                    "Not public course",
                    Response::HTTP_UNAUTHORIZED,
                );
            }
        }
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($course_id);
        $nicknames = [];
        foreach ($tracks as $track) {
            $nicknames[] = $track->getName();
        }
        return new JsonResponse(
            $nicknames,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-courses/names', methods: ['GET'])]
    public function get_course_names(): JsonResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $courseNames = [];
        foreach ($user->getCourses() as $course) {
            $courseNames [] = [
                'id' => $course->getId(),
                'name' => $course->getName(),
            ];
        }
        return new JsonResponse(
            array_reverse($courseNames),
            Response::HTTP_OK
        );
    }

    #[Route('/api/data/{course_id}/kml/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_private_kml(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_kml_file($this->security->getUser(), $course_id, $entityManager, $secret_key);
    }

    #[Route('/api/data/{course_id}/xml/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_private_xml(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_xml_file($this->security->getUser(), $course_id, $entityManager, $secret_key);
    }

    #[Route('/api/data/{course_id}/image/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_private_image(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_image_file($this->security->getUser(), $course_id, $entityManager, $secret_key);
    }

    private static function _removeStartAndEndCurlyBraces(string $input): float
    {
        return (float)substr($input, 1, strlen($input) - 2);
    }
}