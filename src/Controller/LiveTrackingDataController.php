<?php

namespace App\Controller;

use App\Entity\LiveTracking;
use App\Entity\LiveTrackingOrienteer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LiveTrackingDataController extends AbstractController
{
    #[Route('/data/live_tracking/session/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function get_current_live_session($course_id, EntityManagerInterface $entityManager): JsonResponse
    {
        $lives = $entityManager->getRepository(LiveTracking::class)->findByCourse($course_id);
        $size = count($lives);
        if ($size > 0) {
            return new JsonResponse(
                ["live_id" => $lives[$size - 1]->getId()],
                Response::HTTP_OK,
            );
        } else {
            return new JsonResponse(
                null,
                Response::HTTP_NOT_FOUND,
            );
        }
    }

    #[Route('/data/live_tracking/connect', methods: ['POST'])]
    public function connect_orienteer_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $live = $entityManager->getRepository(LiveTracking::class)->find($requestContent->id);
        $liveTrackingOrienteer = new LiveTrackingOrienteer();
        $liveTrackingOrienteer->setLive($live);
        $liveTrackingOrienteer->setNickname($requestContent->nickname);
        $liveTrackingOrienteer->setStartDate(new DateTime('now'));
        $entityManager->persist($liveTrackingOrienteer);
        $entityManager->flush();
        return new JsonResponse(
            ["live_id" => $liveTrackingOrienteer->getId()],
            Response::HTTP_OK,
        );
    }

    /**
     * Keep this route for sake of compatibility (since version  3.12.7, March 27, 2024)
     */
    #[Route('/data/live_tracking/position', methods: ['PATCH'])]
    public function set_orienteer_position_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $orienteerLiveId = $requestContent->id;
        $liveTrackingOrienteer = $entityManager->getRepository(LiveTrackingOrienteer::class)->find($orienteerLiveId);
        if ($liveTrackingOrienteer == null) {
            return new JsonResponse(
                null,
                Response::HTTP_REQUEST_TIMEOUT,
            );
        }
        // Reset start date when receiving first position (after start)
        if ($liveTrackingOrienteer->getLatitudes() == null) {
            $liveTrackingOrienteer->setStartDate(new DateTime('now'));
        }
        $liveTrackingOrienteer->setLatitudes($requestContent->latitude);
        $liveTrackingOrienteer->setLongitudes($requestContent->longitude);
        $entityManager->flush();
        return new JsonResponse();
    }

    #[Route('/data/live_tracking/positions', methods: ['PATCH'])]
    public function set_orienteer_positions_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $orienteerLiveId = $requestContent->id;
        $liveTrackingOrienteer = $entityManager->getRepository(LiveTrackingOrienteer::class)->find($orienteerLiveId);
        if ($liveTrackingOrienteer == null) {
            return new JsonResponse(
                null,
                Response::HTTP_REQUEST_TIMEOUT,
            );
        }
        // Reset start date when receiving first position (after start)
        if ($liveTrackingOrienteer->getLatitudes() == null) {
            $liveTrackingOrienteer->setStartDate(new DateTime('now'));
        }
        $liveTrackingOrienteer->setLatitudes($requestContent->latitudes);
        $liveTrackingOrienteer->setLongitudes($requestContent->longitudes);
        $liveTrackingOrienteer->setScore($requestContent->score);
        $entityManager->flush();
        return new JsonResponse();
    }

    #[Route('/data/live_tracking/closing', methods: ['POST'])]
    public function close_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $orienteerLiveId = $requestContent->id;
        $orienteerLive = $entityManager->getRepository(LiveTrackingOrienteer::class)->find($orienteerLiveId);
        if ($orienteerLive == null) {
            return new JsonResponse(
                null,
                Response::HTTP_BAD_REQUEST,
            );
        }
        $entityManager->remove($orienteerLive);
        $entityManager->flush();
        return new JsonResponse();
    }
}
