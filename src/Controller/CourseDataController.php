<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Event;
use App\Entity\MissingControlPoint;
use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use App\Entity\User;
use App\Model\CreateMissingControlPoint;
use App\Model\CreateTrack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use function dirname;

class CourseDataController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/data/courses', methods: ['GET'])]
    public function get_courses(EntityManagerInterface $entityManager): JsonResponse
    {
        return $this->getAllCourses($entityManager, false);
    }

    #[Route('/data/private-courses', methods: ['GET'])]
    public function get_private_courses(EntityManagerInterface $entityManager): JsonResponse
    {
        return $this->getAllCourses($entityManager, true);
    }

    private function getAllCourses(EntityManagerInterface $entityManager, bool $withPrivateCourses): JsonResponse
    {
        $courses = $entityManager->getRepository(Course::class)->findAll();
        foreach ($courses as $course) {
            if ($course->isClosed() || ($course->isPrivate() && !$withPrivateCourses)) {
                unset($courses[array_search($course, $courses)]);
            }
        }
        $dataCourses = [];
        foreach ($courses as $course) {
            $info = [
                "id" => $course->getId(),
                "name" => $course->getName(),
                "latitude" => $course->getLatitude(),
                "longitude" => $course->getLongitude(),
                "length" => $course->getLength(),
            ];
            if ($course->getClub() !== null) {
                $info["club"] = $course->getClub();
            }
            if ($course->getClubURL() !== null) {
                $info["cluburl"] = $course->getClubURL();
            }
            if ($course->getDiscipline()) {
                $info["discipline"] = $course->getDiscipline();
            }
            if ($course->getTouristic()) {
                $info["touristic"] = 1;
            }
            if ($course->isPrivate()) {
                $info["key"] = 1;
            }
            $dataCourses[] = $info;
        }
        return $this->json($dataCourses);
    }

    #[Route('/data/{course_id}/xml/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_xml(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_xml_file(null, $course_id, $entityManager, $secret_key);
    }

    static public function get_xml_file(?User $user, int $id, EntityManagerInterface $entityManager, string $secret_key): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($id);
        $error = self::_checkPermission($user, $course, $secret_key);
        if ($error != null) {
            return $error;
        }

        self::_updateTheNumberOfDownloads($course, $entityManager);

        $fileContent = $course->getXml();
        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $id . ".xml"
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    #[Route('/data/{course_id}/image/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_image(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_image_file(null, $course_id, $entityManager, $secret_key);
    }

    static public function get_image_file(?User $user, int $course_id, EntityManagerInterface $entityManager, string $secret_key): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        $error = self::_checkPermission($user, $course, $secret_key);
        if ($error != null) {
            return $error;
        }

        $filename = $course->getImage();
        $fileExtension = pathinfo($filename, PATHINFO_EXTENSION);
        $contentType = "image/";
        if ($fileExtension == "png") {
            $contentType .= "png";
        } else {
            $contentType .= "jpeg";
        }

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $course_id . "." . $fileExtension);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Length', filesize($filename));
        $response->headers->set('Cache-Control', 'no-cache, must-revalidate');
        $response->setEtag(strval($course->getCreatedAt()->getTimeStamp()));
        $response->setLastModified($course->getCreatedAt());
        $response->setContent(file_get_contents($filename));
        return $response;
    }

    #[Route('/data/{course_id}/img/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_image_encrypted_archive(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        $error = self::_checkPermission($this->security->getUser(), $course, $secret_key);
        if ($error != null) {
            return $error;
        }

        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2) . '/config/secret/.image.key')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        $data = $this->encryptFile($key, $initializationVector, $course->getImage());
        $gzData = gzencode($data);
        $response = new Response($gzData);
        $compressedFilename = $course_id . ".bin" . ".gz";
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', 'application/octet-stream');
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $compressedFilename);
        $response->headers->set('Content-Disposition', $disposition);
        $fileSize = strlen($gzData);
        $response->headers->set('Content-Length', $fileSize);
        $response->headers->set('x-decompressed-content-length', $fileSize);
        return $response;
    }

    #[Route('/data/{course_id}/kml/{secret_key}', requirements: ['course_id' => '\d+', 'secret_key' => '\d+'], methods: ['GET'])]
    public function get_kml(int $course_id, EntityManagerInterface $entityManager, string $secret_key = ''): Response
    {
        return CourseDataController::get_kml_file(null, $course_id, $entityManager, $secret_key);
    }

    static public function get_kml_file(?User $user, int $course_id, EntityManagerInterface $entityManager, string $secret_key): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        $error = self::_checkPermission($user, $course, $secret_key);
        if ($error != null) {
            return $error;
        }

        $fileContent = $course->getKml();
        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $course_id . ".kml"
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    /**
     * @deprecated (since May 2024). Remove in the future.
     */
    #[Route('/data/send', methods: ['POST'])]
    public function upload_track(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $requestContent = json_decode($request->getContent());
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        return $this->addNewTrack($requestContent, $entityManager);
    }

    #[Route('/data/track', methods: ['POST'])]
    public function upload_track_data(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        return $this->addNewTrack($requestContent, $entityManager);
    }

    /*
    * @deprecated remove in 2025
    */
    #[Route('/data/missing-control-point', methods: ['POST'])]
    public function create_missing_control_point(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $requestData = $request->getContent();
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        return $this->addNewMissingCheckpoint($requestContent, $entityManager);
    }

    #[Route('/data/missing-checkpoint', methods: ['POST'])]
    public function upload_missing_checkpoint(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        return $this->addNewMissingCheckpoint($requestContent, $entityManager);
    }

    #[Route('/data/dem-file/{filename}', methods: ['GET'])]
    public function get_dem_file($filename): Response
    {
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $url = "https://step.esa.int/auxdata/dem/SRTMGL1/" . $filename;
        $contents = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $response = new Response();
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $filename . '"');
        $response->setContent($contents);
        return $response;
    }

    #[Route('/data/community/courses/{token}', requirements: ['token' => '.+'], methods: ['GET'])]
    public function get_all_community_courses(string $token, EntityManagerInterface $entityManager): Response
    {
        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2) . '/config/secret/.image.key')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        $communityName = $this->decryptFile($key, $initializationVector, $token);
        if (empty($communityName) || strlen($communityName) > 256) {
            return new JsonResponse(
                [
                    'message' => "Bad request value",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $courses = $entityManager->getRepository(Course::class)->findBy(array("club" => $communityName));
        $results = [];
        /** @var Course $course */
        foreach ($courses as $course) {
            if ($course->isOpen() || !$course->isPrivate()) {
                $results[] = [
                    "id" => $course->getId(),
                    "name" => $course->getName(),
                    "createdAtInMilliseconds" => $course->getCreatedAt()->getTimeStamp() * 1000,
                    "downloadCount" => $course->getDownloads(),
                    "missingControls" => count($course->getMissingControlPoints()),
                    "touristic" => $course->getTouristic(),
                    "closed" => $course->isClosed() || $course->isPrivate(),
                    "discipline" => $course->getDiscipline(),
                ];
            }
        }
        return new JsonResponse(
            $results,
            Response::HTTP_OK
        );
    }

    #[Route('/data/community/{course_id}/missing-checkpoints/{token}', requirements: ['course_id' => '\d+', 'token' => '.+'], methods: ['GET'])]
    public function community_missing_control_points(int $course_id, string $token, EntityManagerInterface $entityManager): Response
    {
        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2) . '/config/secret/.image.key')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        $club = $this->decryptFile($key, $initializationVector, $token);
        if (empty($club) || strlen($club) > 256) {
            return new JsonResponse(
                [
                    'message' => "Bad request value",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course->getClub() != $club) {
            return new JsonResponse(
                [
                    'message' => "Bad request value",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $checkpoints = $course->getMissingControlPoints();
        $result = [];
        foreach ($checkpoints as $checkpoint) {
            $result[] = [
                "id" => $checkpoint->getId(),
                "checkpointId" => $checkpoint->getControlPointId(),
            ];
        }
        return new  JsonResponse(
            $result,
            Response::HTTP_OK,
        );
    }

    #[Route('/data/community/{checkpoint_id}/delete-missing-checkpoint/{token}', requirements: ['checkpoint_id' => '\d+', 'token' => '.+'], methods: ['DELETE'])]
    public function community_delete_missing_checkpoints(int $checkpoint_id, string $token, EntityManagerInterface $entityManager): Response
    {
        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2) . '/config/secret/.image.key')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        $club = $this->decryptFile($key, $initializationVector, $token);
        $checkpoint = $entityManager->getRepository(MissingControlPoint::class)->find($checkpoint_id);
        $course = $checkpoint->getCourse();
        if ($course->getClub() != $club) {
            return new JsonResponse(
                [
                    'message' => "Bad request value",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $entityManager->remove($checkpoint);
        $entityManager->flush();
        return new  JsonResponse();
    }

    private function addToRelatedEvents(int $trackId, EntityManagerInterface $entityManager): void
    {
        $track = $entityManager->getRepository(Track::class)->find($trackId);
        $events = $entityManager->getRepository(Event::class)->findAll();
        foreach ($events as $event) {
            foreach ($event->getEventCourses() as $eventCourse) {
                if ($eventCourse->getCourse() === $track->getCourse()) {
                    foreach ($event->getParticipants() as $participant) {
                        if ($track->getName() === $participant->getNickname()) {
                            $participantMakeEventCourse = $entityManager->getRepository(ParticipantMakeEventCourse::class)->find(
                                [
                                    "eventCourse" => $eventCourse,
                                    "participant" => $participant
                                ]);
                            if ($participantMakeEventCourse->getTrack() === null) {
                                $participantMakeEventCourse->setTrack($track);
                                $entityManager->flush();
                                $event->updateEventScores($entityManager);
                            }
                        }
                    }
                }
            }
        }
    }

    private function encryptFile($encryptKey, $encryptInitializationVector, $inPath): string
    {
        $sourceFile = file_get_contents($inPath);
        $passphrase = base64_decode($encryptKey);
        $initializationVector = base64_decode($encryptInitializationVector);
        $cipherAlgo = 'aes-256-cbc';
        return openssl_encrypt($sourceFile, $cipherAlgo, $passphrase, 0, $initializationVector);
    }

    private function decryptFile($encryptKey, $encryptInitializationVector, $text): string
    {
        $passphrase = base64_decode($encryptKey);
        $initializationVector = base64_decode($encryptInitializationVector);
        $cipherAlgo = 'aes-256-cbc';
        return openssl_decrypt($text, $cipherAlgo, $passphrase, 0, $initializationVector);
    }

    static private function _checkPermission(?User $user, ?Course $course, string $secret_key): ?Response
    {
        if (!$course) {
            return new JsonResponse(["message" => "Invalid request id"], Response::HTTP_BAD_REQUEST);
        }
        if ($user != null && ($user->isAdmin() || $course->getCreator() === $user)) {
            return null;
        }
        if ($course->isClosed()) {
            return new JsonResponse(["message" => "not a public course"], Response::HTTP_UNAUTHORIZED);
        }
        if (($course->isPrivate() && !$course->checkSecretKey($secret_key))) {
            return new JsonResponse(["message" => "Invalid request id"], Response::HTTP_BAD_REQUEST);
        }
        return null;
    }

    private static function _updateTheNumberOfDownloads(Course $course, EntityManagerInterface $entityManager): void
    {
        $count = $course->getDownloads();
        $course->setDownloads($count + 1);
        $entityManager->persist($course);
        $entityManager->flush();
    }

    private function addNewTrack($requestContent, EntityManagerInterface $entityManager): JsonResponse
    {
        $trackCreator = new CreateTrack($requestContent);
        $trackId = $trackCreator->addTrack($entityManager);
        if ($trackId != null) {
            $this->addToRelatedEvents($trackId, $entityManager);
            return new JsonResponse(
                [
                    'trackId' => $trackId,
                ]
            );
        } else {
            return new JsonResponse(
                [
                    'message' => 'Invalid post contents',
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @param $requestData
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function addNewMissingCheckpoint($requestData, EntityManagerInterface $entityManager): JsonResponse
    {
        $creator = new CreateMissingControlPoint($requestData);
        $isValidControlPoint = $creator->addControlPoint($entityManager);
        if ($isValidControlPoint) {
            return new JsonResponse();
        } else {
            return new JsonResponse(
                [
                    'message' => 'Track already stored',
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
