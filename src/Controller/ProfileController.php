<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\CreateUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\SecurityBundle\Security;

class ProfileController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/api/planner/profile/{planner_id}', requirements: ['planner_id' => '\-?\d+'], methods: ['GET'])]
    public function user_profile(int $planner_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if ($planner_id >= 0 && $user != null && $user->isAdmin()) {
            $user = $entityManager->getRepository(User::class)->find($planner_id);
        }
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        return new JsonResponse(
            [
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'phone' => $user->getPhone(),
                'lastname' => $user->getLastName(),
                'firstname' => $user->getFirstName(),
            ],
            Response::HTTP_OK
        );
    }

    #[Route('/api/planner/profile/modify/{planner_id}', requirements: ['planner_id' => '\-?\d+'], methods: ['PATCH'])]
    public function modify_planner_info(int $planner_id, Request $request, UserPasswordHasherInterface $passwordEncoder, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if ($planner_id >= 0 && $user != null && $user->isAdmin()) {
            $user = $entityManager->getRepository(User::class)->find($planner_id);
        }
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $requestData = json_decode($request->getContent());
        $user->setUsername($requestData->username);
        $user->setPlainPassword($requestData->password);
        $user->setEmail($requestData->email);
        $user->setPhone($requestData->phone);
        $user->setLastName($requestData->lastname);
        $user->setFirstName($requestData->firstname);
        $creator = new CreateUser($user);
        $error = $creator->changeUser($passwordEncoder, $entityManager);
        if ($error) {
            return new JsonResponse(
                $error,
                Response::HTTP_BAD_REQUEST
            );
        }
        return new JsonResponse();
    }
}