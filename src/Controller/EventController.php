<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Event;
use App\Entity\EventCourse;
use App\Entity\Participant;
use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use App\Entity\User;
use App\Model\ManuallySetPenaltyOfParticipantMakeEventCourse;
use App\Model\ModifyEventCourse;
use App\Model\ModifyParticipantMakeEventCourse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class EventController extends AbstractController
{
    const MAX_NUMBER_OF_PARTICIPANTS = 2000;

    #[Route('/api/planner/my-events/{planner_id}', requirements: ['planner_id' => '\-?\d+'], defaults: ['planner_id' => -1], methods: ['GET'])]
    public function myEvents(int $planner_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        if ($planner_id > -1) {
            if ($user->isAdmin()) {
                $user = $entityManager->getRepository(User::class)->find($planner_id);
            } else {
                return new JsonResponse(
                    "Unauthorized request",
                    Response::HTTP_UNAUTHORIZED
                );
            }
        }
        $events = [];
        foreach ($user->getEvents() as $event) {
            $events[] = [
                "id" => $event->getId(),
                "name" => $event->getName(),
            ];
        }
        return new  JsonResponse(
            $events,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-event/create', methods: ['POST'])]
    public function create_event(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $data = json_decode($request->getContent());
        $event = new Event();
        $event->setCreator($user);
        if (strlen($data->name) > 55) {
            return new JsonResponse(
                [
                    "message" => "invalid request id",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $event->setName($data->name);
        $event->setType($data->type);
        $entityManager->persist($event);
        $entityManager->flush();
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/detail/{event_id}', requirements: ['event_id' => '\d+'], methods: ['GET'])]
    public function event_detail(int $event_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null) {
            return new JsonResponse(
                [
                    "message" => "invalid request id",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        if (!in_array($event, $user->getEvents()->toArray()) && !in_array('ROLE_ADMIN', $user->getRoles())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $courses = [];
        foreach ($event->getEventCourses() as $eventCourse) {
            $course = $eventCourse->getCourse();
            $xmlContentsAsString = $course->getXml();
            $checkpoints = Course::extractCheckpointsFromXML(simplexml_load_string($xmlContentsAsString));
            $checkpointIds = [];
            foreach ($checkpoints as $checkpoint) {
                $checkpointIds[] = $checkpoint->id;
            }
            $courses[] = [
                "id" => $eventCourse->getId(),
                "courseId" => $course->getId(),
                "name" => $course->getName(),
                "format" => $eventCourse->getFormat(),
                "maxTime" => $eventCourse->getMaxTime(),
                'missingPunchPenalty' => $eventCourse->getMispunchPenalty(),
                "overtimePenalty" => $eventCourse->getOvertimePenalty(),
                "checkpoints" => $checkpointIds
            ];
        }

        $rankings = $event->computeRanking($entityManager->getRepository(ParticipantMakeEventCourse::class));
        $participants = $event->getParticipants();
        $eventParticipants = [];
        for ($i = 0; $i < count($rankings["rank"]); $i++) {
            $indexParticipant = $rankings["rank"][$i];
            $eventParticipants[] = [
                "id" => $participants[$indexParticipant]->getId(),
                "nickname" => $participants[$indexParticipant]->getNickname(),
                "rank" => $i + 1,
                "progress" => $rankings["progress"][$indexParticipant],
                "totalScore" => $rankings["total_score"][$indexParticipant],
                "totalTime" => $rankings["total_time"][$indexParticipant],
                "grossScore" => $rankings["gross_score"][$indexParticipant],
                "totalPenalty" => $rankings["total_penalty"][$indexParticipant],
                "eventCourse" => $rankings["event_course"][$indexParticipant],
                "modified" => $rankings["modified"][$indexParticipant],
            ];
        }
        return new JsonResponse(
            [
                "id" => $event->getId(),
                "name" => $event->getName(),
                "type" => $event->getType(),
                "eventCourses" => $courses,
                "participants" => $eventParticipants,
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-event/modify-event-track', methods: ['PATCH'])]
    public function modify_detail(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = json_decode($request->getContent());
        $eventCourse = $entityManager->getRepository(EventCourse::class)->find($requestData->event_course_id);
        /** @var User $user */
        $user = $this->getUser();
        if ($eventCourse == null || (!$user->isAdmin() && $eventCourse->getEvent()->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        $participant = $entityManager->getRepository(Participant::class)->find($requestData->participant_id);
        $track = $entityManager->getRepository(Track::class)->find($requestData->track_id);
        $participantMakeEventCourse = $entityManager->getRepository(ParticipantMakeEventCourse::class)->find(
            [
                "eventCourse" => $eventCourse,
                "participant" => $participant,
            ],
        );
        $modifyParticipantMakeEventCourse = new ModifyParticipantMakeEventCourse();
        $modifyParticipantMakeEventCourse->setTrack($track);
        $modifyParticipantMakeEventCourse->setParticipantMakeEventCourse($participantMakeEventCourse);
        $modifyParticipantMakeEventCourse->modify($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/{event_id}/add-penalty', requirements: ['event_id' => '\d+'], methods: ['PATCH'])]
    public function add_manual_penalty_to_participant_course(int $event_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        /** @var User $user */
        $user = $this->getUser();
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $requestData = json_decode($request->getContent());
        $manuallySetPenaltyOfParticipantMakeEventCourse = new ManuallySetPenaltyOfParticipantMakeEventCourse();
        $eventCourse = $entityManager->getRepository(EventCourse::class)->find($requestData->event_course_id);
        $participant = $entityManager->getRepository(Participant::class)->find($requestData->participant_id);
        $manuallySetPenaltyOfParticipantMakeEventCourse->setEventCourse($eventCourse);
        $manuallySetPenaltyOfParticipantMakeEventCourse->setParticipant($participant);
        $manuallySetPenaltyOfParticipantMakeEventCourse->setMispunchCount($requestData->missing_punch_penalty);
        $manuallySetPenaltyOfParticipantMakeEventCourse->setOvertimeCount($requestData->overtime_penalty);

        $participantMakeEventCourseRepository = $entityManager->getRepository(ParticipantMakeEventCourse::class);
        $manuallySetPenaltyOfParticipantMakeEventCourse->modify($event, $entityManager, $participantMakeEventCourseRepository);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/update-ranking/{event_id}', requirements: ['event_id' => '\d+'], methods: ['POST'])]
    public function update_event_ranking(int $event_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $event->updateEventScores($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/delete/{event_id}', requirements: ['event_id' => '\d+'], methods: ['DELETE'])]
    public function deleteEvent(int $event_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $event->remove($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/{event_id}/participants', requirements: ['event_id' => '\d+'], methods: ['GET'])]
    public function my_event_participants(int $event_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $eventParticipants = [];
        $participants = $event->getParticipants();
        /** @var Participant $participant */
        foreach ($participants as $participant) {
            $eventParticipants [] = [
                "id" => $participant->getId(),
                "name" => $participant->getNickname(),
            ];
        }

        return new JsonResponse(
            $eventParticipants,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-event/{event_id}/add-course', requirements: ['event_id' => '\d+'], methods: ['POST'])]
    public function add_course_to_event(int $event_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        $requestData = json_decode($request->getContent());
        $course = $entityManager->getRepository(Course::class)->find($requestData->course_id);
        $eventCourse = new EventCourse();
        $eventCourse->setEvent($event);
        $eventCourse->setCourse($course);
        $eventCourse->setFormat($requestData->format);
        $eventCourse->setMispunchPenalty($requestData->missing_punch_penalty);
        $eventCourse->setOvertimePenalty($requestData->overtime_penalty);
        $eventCourse->setMaxTime($requestData->max_time);
        $entityManager->persist($eventCourse);
        $eventCourse->generateParticipantMakeEventCourse($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/{event_id}/delete-participant/{participant_id}', requirements: ['event_id' => '\d+', 'participant_id' => '\d+'], methods: ['DELETE'])]
    public function delete_participant(int $event_id, int $participant_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $participant = $entityManager->getRepository(Participant::class)->find($participant_id);
        $participant->remove($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/{event_id}/event-courses', requirements: ['event_id' => '\d+'], methods: ['GET'])]
    public function my_event_courses(int $event_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $eventCourses = [];
        foreach ($event->getEventCourses() as $eventCourse) {
            $eventCourses[] = [
                "id" => $eventCourse->getId(),
                "courseId" => $eventCourse->getCourse()->getId(),
                "courseName" => $eventCourse->getCourse()->getName(),
                "format" => $eventCourse->getFormat(),
                "missingPunchPenalty" => $eventCourse->getMispunchPenalty(),
                "overtimePenalty" => $eventCourse->getOvertimePenalty(),
                "maxTime" => $eventCourse->getMaxTime(),
            ];
        }
        return new JsonResponse(
            [
                "name" => $event->getName(),
                "type" => $event->getType(),
                "eventCourses" => $eventCourses,
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-event/modify-course/{event_course_id}', requirements: ['event_course_id' => '\d+'], methods: ['PATCH'])]
    public function modify_event_courses(int $event_course_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $eventCourse = $entityManager->getRepository(EventCourse::class)->find($event_course_id);
        if ($eventCourse == null || (!$user->isAdmin() && $eventCourse->getEvent()->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $modifyEventCourse = new ModifyEventCourse($eventCourse);
        $requestData = json_decode($request->getContent());
        $modifyEventCourse->setFormat($requestData->format);
        $modifyEventCourse->setMissingPunchPenalty($requestData->missing_punch_penalty);
        $modifyEventCourse->setOverTimePenalty($requestData->overtime_penalty);
        $modifyEventCourse->setMaxTimeInMilliseconds($requestData->max_time);
        $modifyEventCourse->modify($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/my-event/remove-course/{event_course_id}', requirements: ['event_course_id' => '\d+'], methods: ['DELETE'])]
    public function remove_course_from_event(int $event_course_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $eventCourse = $entityManager->getRepository(EventCourse::class)->find($event_course_id);
        if ($eventCourse == null || (!$user->isAdmin() && $eventCourse->getEvent()->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $eventCourse->remove($entityManager);
        return new JsonResponse();
    }

    #[Route('/data/events', methods: ['GET'])]
    public function event_list(EntityManagerInterface $entityManager): Response
    {
        $allEvents = $entityManager->getRepository(Event::class)->findAll();
        $events = [];
        foreach ($allEvents as $event) {
            $events[] = array(
                "id" => $event->getId(),
                "name" => $event->getName(),
                "courses" => count($event->getEventCourses()),
                "participants" => count($event->getParticipants()),
            );
        }
        return new JsonResponse(
            $events,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/events/info/{event_id}', requirements: ['event_id' => '\d+'], methods: ['GET'])]
    public function event_info(int $event_id, EntityManagerInterface $entityManager): Response
    {
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null) {
            return new JsonResponse(
                [
                    "message" => "invalid request id",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $courses = [];
        foreach ($event->getEventCourses() as $eventCourse) {
            $courses[] = [
                'id' => $eventCourse->getId(),
                'courseId' => $eventCourse->getCourse()->getId(),
                'name' => $eventCourse->getCourse()->getName(),
                'maxTime' => $eventCourse->getMaxTime(),
                'format' => $eventCourse->getFormat(),
                'missingPunchPenalty' => $eventCourse->getMispunchPenalty(),
                'overtimePenalty' => $eventCourse->getOvertimePenalty()
            ];
        }
        $rankings = $event->computeRanking($entityManager->getRepository(ParticipantMakeEventCourse::class));
        $participants = $event->getParticipants();
        $eventParticipants = [];
        for ($i = 0; $i < count($rankings["rank"]); $i++) {
            $indexParticipant = $rankings["rank"][$i];
            $eventParticipants[] = [
                "id" => $participants[$indexParticipant]->getId(),
                "nickname" => $participants[$indexParticipant]->getNickname(),
                "rank" => $i + 1,
                "progress" => $rankings['progress'][$indexParticipant],
                "totalScore" => $rankings['total_score'][$indexParticipant],
                "totalTime" => $rankings['total_time'][$indexParticipant],
                "grossScore" => $rankings["gross_score"][$indexParticipant],
                "totalPenalty" => $rankings["total_penalty"][$indexParticipant],
                "eventCourse" => $rankings['event_course'][$indexParticipant],
            ];
        }
        return new JsonResponse(
            [
                "id" => $event->getId(),
                "name" => $event->getName(),
                "type" => $event->getType(),
                "courses" => $courses,
                "participants" => $eventParticipants,
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/events/register', methods: ['POST'])]
    public function event_register(Request $request, EntityManagerInterface $entityManager): Response
    {
        $data = json_decode($request->getContent());
        $id = $data->event_id;
        $nickname = $data->nickname;
        if (strlen($nickname) > 50) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }
        $event = $entityManager->getRepository(Event::class)->find($id);
        if ($event == null) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }
        if (count($event->getParticipants()) > self::MAX_NUMBER_OF_PARTICIPANTS) {
            return new JsonResponse(
                strval(self::MAX_NUMBER_OF_PARTICIPANTS),
                Response::HTTP_FORBIDDEN
            );
        }
        if (Participant::addParticipant($nickname, $event, $entityManager)) {
            $event->updateEventScores($entityManager);
            return new JsonResponse();
        } else {
            return new  JsonResponse(
                $nickname,
                Response::HTTP_OK
            );
        }
    }

    #[Route('/api/planner/my-event/{event_id}/add-participants', requirements: ['event_id' => '\d+'], methods: ['POST'])]
    public function add_participant_to_event(int $event_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $requestData = json_decode($request->getContent());
        $erroneousNicknames = [];
        foreach ($requestData->nicknames as $nickname) {
            if (count($event->getParticipants()) > self::MAX_NUMBER_OF_PARTICIPANTS) {
                return new JsonResponse(
                    strval(self::MAX_NUMBER_OF_PARTICIPANTS),
                    Response::HTTP_FORBIDDEN
                );
            }
            if (!Participant::addParticipant($nickname, $event, $entityManager)) {
                $erroneousNicknames[] = $nickname;
            }

        }
        $event->updateEventScores($entityManager);
        if (empty($erroneousNicknames)) {
            return new JsonResponse();
        } else {
            $errorMessage = implode(", ", $erroneousNicknames);
            return new JsonResponse(
                $errorMessage,
                Response::HTTP_OK
            );
        }
    }

    #[Route('/api/planner/my-event/{event_id}/modify-participant', requirements: ['event_id' => '\d+'], methods: ['POST'])]
    public function modify_participant_to_event(int $event_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $event = $entityManager->getRepository(Event::class)->find($event_id);
        if ($event == null || (!$user->isAdmin() && $event->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $requestData = json_decode($request->getContent());
        $participant = new Participant();
        if (Participant::checkNicknameValidity($requestData->newNickname)) {
            $participant->setNickname($requestData->oldNickname);
            $error = $participant->modifyNickname($event, $requestData->newNickname, $entityManager);
            if ($error != null) {
                return new  JsonResponse(["message" => $error], Response::HTTP_BAD_REQUEST);
            } else {
                return new  JsonResponse();
            }
        } else {
            return new  JsonResponse(["message" => "Bad nickname"], Response::HTTP_BAD_REQUEST);
        }
    }
}
