<?php

namespace App\Tests\Entity;

use App\Entity\Event;
use App\Entity\Participant;
use App\Entity\ParticipantMakeEventCourse;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ParticipantTest extends TestCase
{
//    /**
//     * @var EntityManager
//     */
//    private $entityManager;

//    protected function setUp(): void
//    {
//        $kernel = self::bootKernel();
//        $this->entityManager = $kernel->getContainer()
//            ->get('doctrine')
//            ->getManager();
//    }

//    public function testRemove()
//    {
//        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
//        $nbParticipant = count($this->entityManager->getRepository(Participant::class)->findAll());
//        $participant = $this->entityManager->getRepository(Participant::class)->findAll()[0];
//        $participant->remove($this->entityManager);
//        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 2);
//        $this->assertSame($nbParticipant, count($this->entityManager->getRepository(Participant::class)->findAll()) + 1);
//    }

//    public function testCheckNicknameFree()
//    {
//        $participant = new Participant();
//        /** @var Event $event */
//        $event = $this->entityManager->getRepository(Event::class)->findAll()[0];
//        $this->assertTrue($participant->checkUnregisteredNickname("Regis", $event));
//    }

//    public function testCheckNicknameNotFree()
//    {
//        $participant = new Participant();
//        /** @var Event $event */
//        $event = $this->entityManager->getRepository(Event::class)->findAll()[0];
//        $this->assertFalse($participant->checkUnregisteredNickname("Suliac", $event));
//    }

//    public function testCreate()
//    {
//        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
//        $nbParticipant = count($this->entityManager->getRepository(Participant::class)->findAll());
//
//        $participant = new Participant();
//        $participant->setNickname("Regis");
//        /** @var Event $event */
//        $event = $this->entityManager->getRepository(Event::class)->findAll()[0];
//        $participant->create($event, $this->entityManager);
//
//        $this->assertSame($nbParticipantMakeEventCourse + 2, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()));
//        $this->assertSame($nbParticipant + 1, count($this->entityManager->getRepository(Participant::class)->findAll()));
//    }

    public function testNicknameValidityWhenTrue1()
    {
        $this->assertTrue(Participant::checkNicknameValidity("toto 1"));
    }

    public function testNicknameValidityWhenTrue2()
    {
        $this->assertTrue(Participant::checkNicknameValidity("XX'xx-xx_xx.xxÚá"));
    }

    public function testNicknameValidityWhenEmail()
    {
        $this->assertTrue(Participant::checkNicknameValidity("titi@website.fr"));
    }

    public function testNicknameValidityWhenTooLong()
    {
        $this->assertFalse(Participant::checkNicknameValidity("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));
    }

    public function testNicknameValidityWhenBadCharacters()
    {
        $this->assertFalse(Participant::checkNicknameValidity("bad;nickname"));
    }

//    protected function tearDown(): void
//    {
//        parent::tearDown();
//        $this->entityManager->close();
//        $this->entityManager = null;
//    }
}