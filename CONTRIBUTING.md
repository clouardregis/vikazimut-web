# Contribuer au développement du serveur web Vikazimut

## Licence

Le projet est placé sous la licence publique générale GNU amoindrie
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).
Ceci impose que les futurs développements se fassent aussi sous cette licence.

## 1. Installer Symfony et Mysql

### 1.1. Installer Php et Symfony

- Installer Php >=8.2 et tout ce qu'il faut comme extensions pour Symfony
- Installer `composer` de Php
- Installer `symfony 6.4`

Pour cela voir les documentations officielles relatives.

### 1.2. Installer la base de données MySQL

- Installer `MySQL` (voir la documentation officielle).
- Créer un compte dans la base de données.

## 2. Installer la distribution

## 2.1. Code source

L'ensemble du code source du serveur Vikazimut est accessible sur le
GitLab du projet : [https://gitlab.com/clouardregis/vikazimut-server](https://gitlab.com/clouardregis/vikazimut-server).

## 2.2. Installer les bundles et les fichiers d'environnement

- Ajouter le fichier `.env.local` (à récupérer auprès du responsbale du projet)
- Ajouter le compte créé dans MySQL dans le fichier `.env.local`
- Ajouter le dossier `config/secret` (à récupérer auprès du responsbale du projet)
- Installer les bundles&nbsp;:
```shell script
$ php8.2 composer.phar update --with-all-dependencies
```
### 2.3. Récupérer un exemple de base de données
 
- Il est possible de récupérer des données de la base de données du serveur (plusieurs Go)
dans un fichier base.sql et le dossier `upload` avec les images des cartes&nbsp;: 
	- Peupler la base de données : `mysql < base.sql`
	- Ajouter le dossier image `upload` à la racine du projet
- Une autre solution moins volumineuse est de créer ses propres cartes et ajouter ses propres traces à la main.

## 2.3. Réinitialiser les mots de passe

Une fois les données récupérées de la base de données, il faut refaire les données qui sont chiffrées dans la base de données : 
1. Éditer le fichier `Model/CreateUser.php`.
2. Commenter le code de la fonction `changePassword`.
3. Remplacer par `return "toto"` qui devient le nouveau mot de passe.
4. Sur le serveur, demander la réinitialisation du mot de passe.
5. Essayer ensuite de se connecter avec le mot ce passe "toto".

## 2.4. Lancement du serveur en local

```shell script
$ symfony server:start --document-root=<chemin absolu vers le dossier du projet> --no-tls
```

# 3. Développer

## Note

Pour déboguer avec Symfony, ouvrir une fenêtre dans le navigateur : `http://127.0.0.1:8000/_profiler`

Utiliser : LoggerInterface $logger par injection puis $logger->info()...

## 3.1. Test

Les tests utilisent la base de données de test peuplée par les fichiers de fixture `src/DataFixtures/`

Pour réinitialiser et relancer la fixture :

```shell script
$ php8.2 bin/console --env=test doctrine:fixtures:load
```

Installer les tests avec la base de données :

```shell script
$ php8.2 bin/console --env=test doctrine:database:create
$ php8.2 bin/console --env=test doctrine:schema:create
```

# 4. Déployer en production

La version courante du déploiement utilise le script suivant :
```shell script
$ ./deploy.sh
```

Le fichier deploy.sh utilise la commande rsync pour ne copier que les fichiers modifiés :

```
rsync -av ./  u51382475@home468677337.1and1-data.host:~/appli --exclude-from=.gitignore --include=public/build --include=vendor --exclude "uml" --exclude "bin" --exclude=".*" --exclude "*~"
```

Après le déploiement, il faut vider le cache pour repartir sur la nouvelle version&nbsp;:

```shell script
php8.2 bin/console cache:clear --env=prod --no-debug
```

## 4.2. Mise à jour de la version de Symfony

Le fichier `composer.json` référence les paquets utilisés par le projet avec leur numéro de version.

Pour changer la version de Symfony, il faut mettre à jour le numéro de version dans le fichier `composer.json`, puis lancer la commande :

```shell script
$ php8.2-cli composer.phar update --no-dev --with-all-dependencies
```

## 4.3. Mettre à jour la base de données

```shell script
$ php8.2 bin/console doctrine:migrations:migrate
```

## 5. Maintenance du serveur

## 5.1.  Mettre à jour la version du composer

```shell script
$ php8.2 composer.phar self-update
```

## 5.2. Mettre à jour la version de Symfony et les vendors sur le serveur

Modifier le fichier `composer.json` avec les bonnes versions, puis lancer la commande :

```shell script
$ php8.2-cli composer.phar update --no-interaction
```

## 5.3. Nettoyer le serveur

Sur le serveur, exécuter à la demande les scripts Unix suivants :

- bin/clean_images.sh: supprimer les images de carte non utilisées

- bin/clean_tracks.sh: supprimer les traces âgées de plus de 6 mois

- bin/database_size.sh: afficher la taille de la base de données

# 5.4. Nettoyer les migrations

```shell script
$ php8.2-cli bin/console doctrine:migrations:dump-schema
$ php8.2-cli bin/console doctrine:migrations:rollup
```

# 5.5 Useful commands summary

```shell script
php8.2-cli bin/console cache:clear –no-debug

php8.2-cli composer.phar update –no-interaction
php8.2-cli composer.phar update --no-dev --with-all-dependencies

php8.2-cli composer.phar self-update

php8.2-cli composer.phar –version

mysql -V
mysql -s -u dbu466036 -p'!Vikazimut14' -h db5000378847.hosting-data.io -P 3306 -D dbs365783
```